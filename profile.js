//get user details

//syntax: localStorage.getItem(<key>);
let token = localStorage.getItem('token');
console.log(token);

let nameDiv = document.querySelector("#profile-name");
let emailDiv = document.querySelector("#profile-email");
let mobileDiv = document.querySelector("#profile-mobile");


fetch('http://localhost:4000/users/getUserDetails',{

	headers: {
		"Authorization": `Bearer ${token}`

	}

})

.then(res => res.json())
.then(data => {

	console.log(data);
	nameDiv.innerHTML = `Hi, ${data.firstName} ${data.lastName}` 
	emailDiv.innerHTML = `Email: ${data.email}` 
	mobileDiv.innerHTML = `Mobile: ${data.mobileNo}`

})