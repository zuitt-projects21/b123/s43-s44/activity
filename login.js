//"/login"

let emailInput = document.querySelector("#email-input");
let passwordInput = document.querySelector("#password-input");

document.querySelector('#form-login').addEventListener('submit',(e)=>{

	//event object - contains details about our event and is received by any function added in an addEventListener. This event object contains details about the exact event, details about which element triggered the event, it even has details abt the values of the element that triggered it.


	//submit will, by default, refresh your page instead.	
	e.preventDefault()

	fetch('http://localhost:4000/users/login/',{

		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({

			email: emailInput.value,
			password: passwordInput.value

		})
	})
		.then(res=> res.json())
		.then(data => {
			/*console.log(data);*/
			/*console.log(data.accessToken);*/
			//localStorage - is an object in JS which will allow us to save small amts of data within our browsers. We can use this to save our token. localStorage exists in most browsers.

			//localStorage.setItem() will allow us ti save data in our browsers. however, any data we pass to our localStorage will become a string.
			//syntax: localStorage.setItem(<key>,<value>)
			localStorage.setItem('token',data.accessToken)

 
		})
})