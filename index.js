let coursesDiv = document.querySelector("#courses-div");

console.log(coursesDiv);
/*console.log(coursesDiv.innerHTML);

let mainDiv = document.querySelector("#main-div");
console.log(mainDiv.innerHTML);

mainDiv.innerHTML = "<h1>Batch 123 is awesome!</h1>"
mainDiv.innerHTML += "<p>Full Stack Developers!</p>"
mainDiv.innerHTML =  mainDiv.innerHTML + "<p>I am poof</p>"*/

//page requesting data from backend.
//fetch() is a javascript method which allows us to pass or create a request to an api.
	//you can pass arguments here
//syntax fetch(<requestURL>)
//.then() allows us to handle/process the result of a previous function
//.then(res=> res.json()) - it handles/processes the server's response and turns the response into a proper JS object
fetch('http://localhost:4000/courses/getActiveCourses')
	//.then(res=> console.log(res)) - complete response(headers,body, etc)
	//so you have to convert it to json using json method
.then(res=>res.json())
	//result of this res.json of response and getting the proper data we asked from our server.
.then(data => {

	console.log(data);
	//note: you cannot use forEach for anything except arrays
	let courseCards = "";
	data.forEach((course)=>{

		console.log(course);
		//add a div for each item in our array:
		courseCards += `

		<div class="card">
			<h4>${course.name}</h4>
			<p>${course.description}</p>
			<span>Price: PHP ${course.price}</span>
		</div>

		`

	})

	coursesDiv.innerHTML = courseCards;
})	


