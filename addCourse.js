let courseNameInput = document.querySelector("#course-name-input");
let descriptionInput = document.querySelector("#description-input");
let priceInput = document.querySelector("#price-input");

let token = localStorage.getItem('token');

document.querySelector('#form-addCourse').addEventListener('submit', (e)=>{
	e.preventDefault()

	fetch('http://localhost:4000/courses/',{

		method: 'POST',
		headers: {
			"Authorization": `Bearer ${token}`,
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({

			name: courseNameInput.value,
			description: descriptionInput.value,
			price: priceInput.value

		})


	})
	.then(res => res.json())
	.then(data => {
		console.log(data);
	})

})